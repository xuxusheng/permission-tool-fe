import * as React from "react";
import { render } from "react-dom";

import "./index.global.scss";

import { AppWithProvider } from "./redux";

const rootEl = document.querySelector("#root");

render(<AppWithProvider />, rootEl);
