export type ILoading<T> = { [K in keyof T]?: boolean };
