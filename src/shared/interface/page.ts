export interface IPageInfo {
  pn: number;
  ps: number;
  total: number;
}

export type DwithP<T> = { data: T } & IPageInfo;
