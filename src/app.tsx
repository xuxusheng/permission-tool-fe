import * as React from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from "react-router-dom";

import { Admin } from "./pages/admin";
import { Main } from "./pages/main";

export class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact={true} component={Main} />
          <Route path="/admin" component={Admin} />
          <Redirect to="/" />
        </Switch>
      </Router>
    );
  }
}
