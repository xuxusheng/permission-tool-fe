import * as React from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { History } from "history";

const style = require("./index.scss");

class MainBase extends React.PureComponent<
  RouteComponentProps<{}, {}> & {
    history: History;
  }
> {
  enter = () => {
    this.props.history.push("/admin");
  };

  render() {
    return (
      <div className={style.wrap}>
        <div className={style.store}>
          <div className={style.awning} />
          <div className={style.window} />
          <div
            className={style.door}
            onClick={() => {
              this.enter();
            }}
          />
        </div>
        <div className={style.cloud} />
      </div>
    );
  }
}

export const Main = withRouter(MainBase);
