import * as React from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import { AdminLayout } from "./containers/layout";
import { AggRole } from "./containers/agg-role";
import { AggPermission } from "./containers/agg-permission";
import { AggPermissionControl } from "./containers/agg-permission-control";

export class Admin extends React.PureComponent {
  render() {
    return (
      <AdminLayout>
        <Switch>
          <Route path="/admin/agg-permission" component={AggPermission} />
          <Route path="/admin/agg-role" component={AggRole} />
          <Route
            path="/admin/agg-permission-control"
            component={AggPermissionControl}
          />
          <Redirect to="/admin/agg-permission" />
        </Switch>
      </AdminLayout>
    );
  }
}
