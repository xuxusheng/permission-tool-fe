import { DwithP } from "../../../shared/interface";

export interface IRole {
  id: number;
  roleName: string;
  description: string;
  createdAt: string;
  updatedAt: string;
}

type RoleList = IRole[];

export type RoleListWP = DwithP<RoleList>;
