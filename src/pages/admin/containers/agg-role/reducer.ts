import { RoleListWP } from "../../interface";
import { ILoading } from "../../../../shared/interface";
import { Actions, ActTypes } from "./actions";

export { epics } from "./epics";
export { ActTypes } from "./actions";

export const NAME = "aggRole";

interface DataState {
  query: string;
  roleList: RoleListWP;
}

export type Loading = ILoading<DataState>;

export type ShowModal = "" | "create" | "update";

interface UIState {
  showModal: ShowModal;
  loading: Loading;
}

export type StoreState = DataState & UIState;

const initialState: StoreState = {
  showModal: "",
  loading: {},
  query: "",
  roleList: {
    data: [],
    pn: 1,
    ps: 10,
    total: 0
  }
};

export function reducer(
  state: StoreState = initialState,
  action: Actions
): StoreState {
  switch (action.type) {
    case ActTypes.SetLoading:
      return { ...state, loading: { ...state.loading, ...action.payload } };
    case ActTypes.ToggleShowModal:
      return { ...state, showModal: action.payload };
    case ActTypes.SetQuery:
      return { ...state, query: action.payload };
    case ActTypes.SetRoleList:
      return { ...state, roleList: action.payload };
    default:
      return state;
  }
}
