import { Action } from "redux";
import { RoleListWP } from "../../interface";
import { Loading, ShowModal } from "./reducer";

export enum ActTypes {
  Nil,
  ToggleShowModal,
  SetLoading,
  SetQuery,
  GetRoleListAsync,
  SetRoleList,
  CreateRoleAsync
}

export class ActionNil implements Action {
  readonly type = ActTypes.Nil;
}

export class ActionToggleShowModal implements Action {
  readonly type = ActTypes.ToggleShowModal;

  constructor(public payload: ShowModal) {}
}

export class ActionSetLoading implements Action {
  readonly type = ActTypes.SetLoading;

  constructor(public payload: Loading) {}
}

export class ActionSetQuery implements Action {
  readonly type = ActTypes.SetQuery;

  constructor(public payload: string) {}
}

export class ActionGetRoleListAsync implements Action {
  readonly type = ActTypes.GetRoleListAsync;

  constructor(
    public payload: {
      query: string;
      pn: number;
      ps: number;
    }
  ) {}
}

export class ActionCreateRoleAsync implements Action {
  readonly type = ActTypes.CreateRoleAsync;

  constructor(payload: { name: string; description: string }) {}
}

export class ActionSetRoleList implements Action {
  readonly type = ActTypes.SetRoleList;

  constructor(public payload: RoleListWP) {}
}

export type Actions =
  | ActionNil
  | ActionToggleShowModal
  | ActionSetLoading
  | ActionSetQuery
  | ActionGetRoleListAsync
  | ActionSetRoleList
  | ActionCreateRoleAsync;
