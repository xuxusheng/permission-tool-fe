import * as React from "react";
import { Button, Input, Modal, Table } from "antd";
import { RoleListWP } from "../../interface";
import { Loading, NAME, ShowModal, StoreState } from "./reducer";
import { ActionGetRoleListAsync, ActionToggleShowModal } from "./actions";
import { connect } from "react-redux";
import { RoleModal } from "./components/role-modal";

interface IStateToProps {
  loading: Loading;
  showModal: ShowModal;
  query: string;
  roleList: RoleListWP;
}

interface IDispatchToProps {
  toggleShowModal(showModal: ShowModal): void;

  getRoleList(query: string, pn: number, ps: number): void;
}

interface IState {
  query: string;
  updateRole: {
    name: string;
    description: string;
  };
}

const mapStateToProps = (rootStoreState, ownProps): IStateToProps => {
  const state: StoreState = rootStoreState[NAME];
  const { loading, showModal, query, roleList } = state;
  return {
    ...ownProps,
    loading,
    showModal,
    query,
    roleList
  };
};

const mapDispatchToProps = (dispatch): IDispatchToProps => {
  return {
    toggleShowModal: showModal =>
      dispatch({
        ...new ActionToggleShowModal(showModal)
      }),
    getRoleList: (query, pn, ps) =>
      dispatch({
        ...new ActionGetRoleListAsync({ query, pn, ps })
      })
  };
};

class AggRoleBase extends React.PureComponent<
  IStateToProps & IDispatchToProps,
  IState
> {
  state = {
    query: "",
    updateRole: {
      name: "",
      description: ""
    }
  };

  componentDidMount() {
    const { ps } = this.props.roleList;
    this.props.getRoleList("", 1, ps);
  }

  onSearch() {
    const { query } = this.state;
    const { ps } = this.props.roleList;
    this.props.getRoleList(query, 1, ps);
  }

  onPageChange(page) {
    const { current, pageSize } = page;
    const { query } = this.props;
    this.props.getRoleList(query, current, pageSize);
  }

  toggleShowModal(showModal: ShowModal) {
    if (!showModal) {
      // todo 清空
    }
    this.props.toggleShowModal(showModal);
  }

  getColumnsItem = () => {
    return [
      {
        title: "id",
        dataIndex: "id",
        key: "id"
      },
      {
        title: "角色名",
        dataIndex: "roleName",
        key: "roleName"
      },
      {
        title: "描述",
        dataIndex: "description",
        key: "description"
      },
      {
        title: "更新日期",
        dataIndex: "updatedAt",
        key: "updatedAt"
      },
      {
        title: "创建日期",
        dataIndex: "createdAt",
        key: "createdAt"
      }
    ];
  };

  getCreateModalItem = () => {
    const { showModal } = this.props;

    return (
      <Modal
        title="添加角色"
        visible={showModal === "create"}
        onOk={() => {}}
        afterClose={() => this.toggleShowModal("")}
        onCancel={() => this.toggleShowModal("")}
      >
        <span>create</span>
      </Modal>
    );
  };

  getUpdateModalItem = () => {
    const { showModal } = this.props;
  };

  render() {
    const { roleList, loading, showModal } = this.props;
    const { data, pn, ps, total } = roleList;
    const { query, updateRole } = this.state;
    return (
      <div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Input.Search
            placeholder="请输入角色名"
            style={{ width: "200px", marginBottom: "16px" }}
            enterButton={true}
            value={query}
            onChange={e => this.setState({ query: e.target.value })}
            onSearch={() => this.onSearch()}
          />
          <Button onClick={() => this.toggleShowModal("create")}>新建</Button>
        </div>

        <Table
          columns={this.getColumnsItem()}
          rowKey={"id"}
          pagination={{
            current: pn,
            pageSize: ps,
            total,
            showSizeChanger: true,
            pageSizeOptions: ["5", "10", "20", "30"]
          }}
          onChange={page => this.onPageChange(page)}
          loading={loading.roleList}
          dataSource={data}
        />

        <RoleModal
          title="添加角色"
          visible={showModal === "create"}
          onOk={() => {
            console.log("create");
          }}
          onCancel={() => this.toggleShowModal("")}
          afterClose={() =>
            this.setState({
              updateRole: { name: "", description: "" }
            })
          }
        >
          <span>create</span>
        </RoleModal>

        <RoleModal
          title="修改角色"
          role={updateRole}
          visible={showModal === "update"}
          onOk={() => {
            console.log("update");
          }}
          onCancel={() => this.toggleShowModal("")}
          afterClose={() =>
            this.setState({
              updateRole: { name: "", description: "" }
            })
          }
        >
          <span>update</span>
        </RoleModal>
      </div>
    );
  }
}

export const AggRole = connect(
  mapStateToProps,
  mapDispatchToProps
)(AggRoleBase);
