interface Nav {
  name: string;
  icon: string;
  path?: string;
  children?: SubNav[];
}

interface SubNav {
  name: string;
  path: string;
}

export const navData: Nav[] = [
  {
    name: "首页",
    icon: "home",
    path: "/main"
  },
  {
    name: "agg 权限管理",
    icon: "database",
    children: [
      {
        name: "角色管理",
        path: "/admin/agg-role"
      },
      {
        name: "权限管理",
        path: "/admin/agg-permission"
      },
      {
        name: "权限控制",
        path: "/admin/agg-permission-control"
      }
    ]
  }
];
