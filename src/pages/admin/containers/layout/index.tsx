import * as React from "react";
import { Icon, Layout, Menu } from "antd";
import { NavLink, RouteComponentProps, withRouter } from "react-router-dom";
import { navData } from "./nav";

const { Sider, Content, Footer } = Layout;
const { SubMenu } = Menu;
const classNames = require("classnames");
const style = require("./index.scss");

class AdminLayoutBase extends React.PureComponent<RouteComponentProps<any>> {
  getMenuItem() {
    return navData.map(nav => {
      if (nav.children) {
        return (
          <SubMenu
            key={nav.name}
            title={
              <span className={style["sub-menu-title"]}>
                {nav.icon ? <Icon type={nav.icon} /> : ""}
                <span>{nav.name}</span>
              </span>
            }
          >
            {nav.children
              ? nav.children.map(subNav => (
                  <Menu.Item key={subNav.name}>
                    <NavLink
                      to={subNav.path}
                      className={style["sub-nav-item"]}
                      activeClassName={style.active}
                    >
                      {subNav.name}
                    </NavLink>
                  </Menu.Item>
                ))
              : ""}
          </SubMenu>
        );
      }
      return (
        <Menu.Item key={nav.name}>
          <NavLink
            to={nav.path || ""}
            className={classNames(style["nav-item"], style["sub-nav-item"])}
            activeClassName={style.active}
          >
            <Icon type={nav.icon} />
            {nav.name}
          </NavLink>
        </Menu.Item>
      );
    });
  }

  render() {
    return (
      <Layout>
        <Sider breakpoint="md" collapsedWidth="0">
          <div className={style.logo}>
            <Icon type="gift" style={{ marginRight: "8px" }} />
            xs'admin
          </div>
          <Menu className={style.menu} theme="dark" mode="inline">
            {this.getMenuItem()}
          </Menu>
        </Sider>
        <Layout>
          {/*<Header style={{ background: '#fff', padding: 0 }}/>*/}
          <Content style={{ margin: "24px 16px 0" }}>
            <div
              style={{
                padding: 24,
                background: "#fff",
                minHeight: "calc(100vh - 24px - 69px)"
              }}
            >
              {this.props.children}
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            powered by <a href={"https://xuxusheng.github.io"}>XuSheng</a>
          </Footer>
        </Layout>
      </Layout>
    );
  }
}

export const AdminLayout = withRouter(AdminLayoutBase);
