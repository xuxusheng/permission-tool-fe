import { Api } from "../../../shared/api";
import { RoleListWP } from "../interface";

class RoleService extends Api {
  getRoleList({ query, pn, ps }: { query: string; pn: number; ps: number }) {
    return this.request<RoleListWP>({
      url: "/role",
      method: "get",
      params: {
        name: query,
        pn,
        ps
      }
    })(require("Schema/role/get-role-list"));
  }
}

export const adminService = new RoleService();
