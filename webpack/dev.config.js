const path = require("path");
const merge = require("webpack-merge");
const { APP_NAME } = require("./env");

module.exports = (env, argv) => {
  const config = {};

  config.devtool = "inline-source-map";

  config.devServer = {
    historyApiFallback: {
      index: path.posix.join("/static", APP_NAME, "index.html")
    },
    progress: true, // show progress on chrome devtool
    proxy: {
      "/api": {
        target: "http://localhost:3000",
        pathRewrite: {
          "^/api": "/permission-tool"
        }
      }
    }
  };

  return merge(require("./common.config")(env, argv), config);
};
