const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const tsImportPluginFactory = require("ts-import-plugin");
const { APP_NAME } = require("./env");

module.exports = (env, argv) => {
  const mode = argv.mode;
  const isProd = mode === "production";
  const config = {};

  config.entry = {
    app: path.join(__dirname, "../src", "index.tsx")
  };

  config.resolve = {
    // 尽量减少 resolve.modules, resolve.extensions, resolve.mainFiles, resolve.descriptionFiles 中类目的数量，因为他们会增加文件系统调用的次数
    alias: {
      Schema: path.resolve(__dirname, "../src/shared/schema/")
    },
    extensions: [".js", ".ts", ".jsx", ".tsx", ".yml"]
  };

  config.output = {
    filename: isProd ? "[name].[hash].js" : "[name].js",
    path: path.join(__dirname, "../dist/static", APP_NAME),
    publicPath: path.posix.join("/static/", APP_NAME, "/")
  };

  const styleLoader = isProd ? MiniCssExtractPlugin.loader : "style-loader";

  config.module = {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          transpileOnly: true,
          getCustomTransformers: () => ({
            before: [
              tsImportPluginFactory({
                libraryName: "antd",
                libraryDirectory: "lib",
                style: true
              })
            ]
          }),
          compilerOptions: {
            module: "es2015"
          }
        },
        exclude: /node_modules/
      },
      {
        test: /\.global\.(sa|sc|c)ss$/,
        use: [
          {
            loader: styleLoader
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: !isProd
            }
          },
          "postcss-loader",
          "sass-loader"
        ],
        exclude: /node_modules/
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: styleLoader
          },
          {
            loader: "css-loader",
            options: {
              modules: true,
              sourceMap: !isProd
            }
          },
          "postcss-loader",
          "sass-loader"
        ],
        exclude: [/node_modules/, /\.global\.scss$/]
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: styleLoader
          },
          "css-loader",
          "sass-loader"
        ],
        include: /node_modules/
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: styleLoader
          },
          "css-loader",
          {
            loader: "less-loader",
            options: {
              noIeCompat: true,
              javascriptEnabled: true
            }
          }
        ],
        include: /node_modules/
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)/,
        use: ["file-loader"]
      },
      {
        test: /\.ya?ml$/,
        use: ["json-loader", "yaml-loader"]
      }
    ]
  };

  config.plugins = [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, "../src/index.html")
    })
  ];

  return config;
};
